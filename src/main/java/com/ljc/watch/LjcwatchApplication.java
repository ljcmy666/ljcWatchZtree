package com.ljc.watch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LjcwatchApplication {

	public static void main(String[] args) {
        
		SpringApplication.run(LjcwatchApplication.class, args);
	}
}
